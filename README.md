# Vault HA - Website
This is a simple website for the Project "Vault HA" which you can find under: [vault-ha.de](https://vault-ha.de/)

## local development
just use docker and nginx:

```
docker run --rm -p80:80 -v $(pwd)/src:/usr/share/nginx/html nginx
```