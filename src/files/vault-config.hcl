ui = true

backend "consul" {
  address = "127.0.0.1:8500"
  token = "398073a8-5091-4d9c-871a-bbbeb030d1f6"
  path = "vault/"
}
 
listener "tcp" {
  address = "0.0.0.0:8200"
  tls_disable = "true"
}
